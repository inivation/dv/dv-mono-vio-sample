## Changes

* Implementing `addKeypoints` function so external trackers can be used instead of the frontend processing.
* Replacing `okvis::Frontend` with `okvis::NoProcessingFrontend` that does not run feature detection and tracking, just performs track association.
* Adding `setZeroSpeedPrior` function to be able to set high-confidence zero speed assumption depending on event rate.
