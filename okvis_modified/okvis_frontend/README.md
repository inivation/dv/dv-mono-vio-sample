## Changes

* Replacing feature distance calculation by removing brisk feature distance and just returning 0 distance if track IDs match and the triangulated position passes validity check.
* Removing `okvis::Frontend` from compilation, since we don't want dependency on brisk library.
* Implement `okvis::NoProcessingFrontend` suitable for use with externally supplied feature tracks.
