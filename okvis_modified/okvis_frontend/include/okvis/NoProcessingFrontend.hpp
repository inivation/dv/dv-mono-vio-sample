#pragma once

#include <okvis/DenseMatcher.hpp>
#include <okvis/Estimator.hpp>
#include <okvis/VioFrontendInterface.hpp>
#include <okvis/assert_macros.hpp>
#include <okvis/timing/Timer.hpp>
#include <okvis/DenseMatcher.hpp>
#include <okvis/VioKeyframeWindowMatchingAlgorithm.hpp>

#include <boost/lockfree/spsc_queue.hpp>

#include <mutex>
#include <ostream>
#include <unordered_map>

/// \brief okvis Main namespace of this package.
namespace okvis {

/**
 * @brief A frontend using Vilib
 */
class NoProcessingFrontend : public VioFrontendInterface {
public:
    OKVIS_DEFINE_EXCEPTION(Exception, std::runtime_error)

#ifdef DEACTIVATE_TIMERS
    typedef okvis::timing::DummyTimer TimerSwitchable;
#else
    typedef okvis::timing::Timer TimerSwitchable;
#endif


    /**
     * @brief Constructor with no front
     * @param cameraNum       Vio parameters
     */
    explicit NoProcessingFrontend(size_t numCameras);


    ///@{
    /**
     * @brief Detection and descriptor extraction on a per image basis.
     * @remark This method is threadsafe.
     * @param cameraIndex Index of camera to do detection and description.
     * @param frameOut    Multiframe containing the frames.
     *                    Resulting keypoints and descriptors are saved in here.
     * @param T_WC        Pose of camera with index cameraIndex at image capture time.
     * @param[in] keypoints If the keypoints are already available from a different source, provide them here
     *                      in order to skip detection.
     * @warning Using keypoints from a different source is not yet implemented.
     * @return True if successful.
     */
    bool detectAndDescribe(size_t cameraIndex,
                           std::shared_ptr<okvis::MultiFrame> frameOut,
                           const okvis::kinematics::Transformation &T_WC,
                           const std::vector<cv::KeyPoint> *keypoints) override;

    /**
     * @brief Matching as well as initialization of landmarks and state.
     * @warning This method is not threadsafe.
     * @warning This method uses the estimator. Make sure to not access it in another thread.
     * @param estimator       Estimator.
     * @param T_WS_propagated Pose of sensor at image capture time.
     * @param params          Configuration parameters.
     * @param map             Unused.
     * @param framesInOut     Multiframe including the descriptors of all the keypoints.
     * @param[out] asKeyframe Should the frame be a keyframe?
     * @return True if successful.
     */
    bool dataAssociationAndInitialization(
        okvis::Estimator &estimator,
        okvis::kinematics::Transformation &T_WS_propagated,
        const okvis::VioParameters &params,
        const std::shared_ptr<okvis::MapPointVector> map,
        std::shared_ptr<okvis::MultiFrame> framesInOut, bool *asKeyframe) override;

    /**
     * @brief Propagates pose, speeds and biases with given IMU measurements.
     * @see okvis::ceres::ImuError::propagation()
     * @remark This method is threadsafe.
     * @param[in] imuMeasurements All the IMU measurements.
     * @param[in] imuParams The parameters to be used.
     * @param[inout] T_WS_propagated Start pose.
     * @param[inout] speedAndBiases Start speed and biases.
     * @param[in] t_start Start time.
     * @param[in] t_end End time.
     * @param[out] covariance Covariance for GIVEN start states.
     * @param[out] jacobian Jacobian w.r.t. start states.
     * @return True on success.
     */
    bool propagation(const okvis::ImuMeasurementDeque &imuMeasurements,
                     const okvis::ImuParameters &imuParams,
                     okvis::kinematics::Transformation &T_WS_propagated,
                     okvis::SpeedAndBias &speedAndBiases,
                     const okvis::Time &t_start, const okvis::Time &t_end,
                     Eigen::Matrix<double, 15, 15> *covariance,
                     Eigen::Matrix<double, 15, 15> *jacobian) const override;

    /// @brief Get the area overlap threshold under which a new keyframe is inserted.
    [[nodiscard]] float getKeyframeInsertionOverlapThershold() const {
        return keyframeInsertionOverlapThreshold_;
    }

    /// @brief Get the matching ratio threshold under which a new keyframe is inserted.
    [[nodiscard]] float getKeyframeInsertionMatchingRatioThreshold() const {
        return keyframeInsertionMatchingRatioThreshold_;
    }

    /// @brief Returns true if the initialization has been completed (RANSAC with actual translation)
    [[nodiscard]] bool isInitialized() const {
        return isInitialized_;
    }

    /// @brief Set the area overlap threshold under which a new keyframe is inserted.
    void setKeyframeInsertionOverlapThreshold(float threshold) {
        keyframeInsertionOverlapThreshold_ = threshold;
    }

    /// @brief Set the matching ratio threshold under which a new keyframe is inserted.
    void setKeyframeInsertionMatchingRatioThreshold(float threshold) {
        keyframeInsertionMatchingRatioThreshold_ = threshold;
    }

    /// @}

    /**
     * Return a list of track ids that have been rejected for more then "rejectCount" times.
     * @param cameraIndex       Camera id
     * @param rejectCount       Reject count limit, if the limit is reached, the track id will be inserted into list
     *                          and removed from counter memory
     * @return                  A list of track ids that are over the reject count limit
     */
    std::vector<int> getCameraTrackRejections(size_t cameraIndex, int rejectCount = 5);


private:
    std::vector<std::unordered_map<int, int>> trackRejectionCounters;

    std::vector<std::unique_ptr<boost::lockfree::spsc_queue<int>>> rejectTrack;

    std::atomic<bool> isInitialized_;        ///< Is the pose initialised?
    const size_t numCameras_;   ///< Number of cameras in the configuration.

    std::unique_ptr<okvis::DenseMatcher> matcher_; ///< Matcher object.

    /**
     * @brief If the hull-area around all matched keypoints of the current frame (with existing landmarks)
     *        divided by the hull-area around all keypoints in the current frame is lower than
     *        this threshold it should be a new keyframe.
     * @see   doWeNeedANewKeyframe()
     */
    float keyframeInsertionOverlapThreshold_;  //0.6
    /**
     * @brief If the number of matched keypoints of the current frame with an older frame
     *        divided by the amount of points inside the convex hull around all keypoints
     *        is lower than the threshold it should be a keyframe.
     * @see   doWeNeedANewKeyframe()
     */
    float keyframeInsertionMatchingRatioThreshold_;  //0.2

    /**
     * @brief Decision whether a new frame should be keyframe or not.
     * @param estimator     const reference to the estimator.
     * @param currentFrame  Keyframe candidate.
     * @return True if it should be a new keyframe.
     */
    [[nodiscard]] bool doWeNeedANewKeyframe(const okvis::Estimator &estimator,
                                            std::shared_ptr<okvis::MultiFrame> currentFrame) const;  // based on some overlap area heuristics

    /**
     * @brief Match a new multiframe to existing keyframes
     * @tparam MATCHING_ALGORITHM Algorithm to match new keypoints to existing landmarks
     * @warning As this function uses the estimator it is not threadsafe
     * @param      estimator              Estimator.
     * @param[in]  params                 Parameter struct.
     * @param[in]  currentFrameId         ID of the current frame that should be matched against keyframes.
     * @param[out] rotationOnly           Was the rotation only RANSAC motion model good enough to
     *                                    explain the motion between the new frame and the keyframes?
     * @param[in]  usePoseUncertainty     Use the pose uncertainty for the matching.
     * @param[out] uncertainMatchFraction Return the fraction of uncertain matches. Set to nullptr if not interested.
     * @param[in]  removeOutliers         Remove outliers during RANSAC.
     * @return The number of matches in total.
     */
    template<class MATCHING_ALGORITHM>
    int matchToKeyframes(okvis::Estimator &estimator,
                         const okvis::VioParameters &params,
                         uint64_t currentFrameId, bool &rotationOnly,
                         bool usePoseUncertainty = true,
                         bool removeOutliers = true);  // for wide-baseline matches (good initial guess)

    /**
     * @brief Match a new multiframe to the last frame.
     * @tparam MATCHING_ALGORITHM Algorithm to match new keypoints to existing landmarks
     * @warning As this function uses the estimator it is not threadsafe.
     * @param estimator           Estimator.
     * @param params              Parameter struct.
     * @param currentFrameId      ID of the current frame that should be matched against the last one.
     * @param usePoseUncertainty  Use the pose uncertainty for the matching.
     * @param removeOutliers      Remove outliers during RANSAC.
     * @return The number of matches in total.
     */
    template<class MATCHING_ALGORITHM>
    int matchToLastFrame(okvis::Estimator &estimator,
                         const okvis::VioParameters &params,
                         uint64_t currentFrameId,
                         bool usePoseUncertainty = true,
                         bool removeOutliers = true);

    /**
     * @brief Match the frames inside the multiframe to each other to initialise new landmarks.
     * @tparam MATCHING_ALGORITHM Algorithm to match new keypoints to existing landmarks.
     * @warning As this function uses the estimator it is not threadsafe.
     * @param estimator   Estimator.
     * @param multiFrame  Multiframe containing the frames to match.
     */
    template<class MATCHING_ALGORITHM>
    void matchStereo(okvis::Estimator &estimator,
                     std::shared_ptr<okvis::MultiFrame> multiFrame);

    /**
     * @brief Perform 3D/2D RANSAC.
     * @warning As this function uses the estimator it is not threadsafe.
     * @param estimator       Estimator.
     * @param nCameraSystem   Camera configuration and parameters.
     * @param currentFrame    Frame with the new potential matches.
     * @param removeOutliers  Remove observation of outliers in estimator.
     * @return Number of inliers.
     */
    int runRansac3d2d(okvis::Estimator &estimator,
                      const okvis::cameras::NCameraSystem &nCameraSystem,
                      std::shared_ptr<okvis::MultiFrame> currentFrame,
                      bool removeOutliers);

    /**
     * @brief Perform 2D/2D RANSAC.
     * @warning As this function uses the estimator it is not threadsafe.
     * @param estimator         Estimator.
     * @param params            Parameter struct.
     * @param currentFrameId    ID of the new multiframe containing matches with the frame with ID olderFrameId.
     * @param olderFrameId      ID of the multiframe to which the current frame has been matched against.
     * @param initializePose    If the pose has not yet been initialised should the function try to initialise it.
     * @param removeOutliers    Remove observation of outliers in estimator.
     * @param[out] rotationOnly Was the rotation only RANSAC model enough to explain the matches.
     * @return Number of inliers.
     */
    int runRansac2d2d(okvis::Estimator &estimator,
                      const okvis::VioParameters &params, uint64_t currentFrameId,
                      uint64_t olderFrameId, bool initializePose,
                      bool removeOutliers, bool &rotationOnly) const;


    template<class MATCHING_ALGORITHM>
    size_t doMatchingBetweenFrames(
        okvis::Estimator& estimator,
        int matchType,
        uint64_t currentFrameId,
        uint64_t olderFrameId,
        size_t cameraId,
        bool usePoseUncertainty);
};

}  // namespace okvis

