## Changes

* Fixed a C++17 compatibility issue with throwing exceptions in function `okvis_walltime`.
* Adding a convenience method `fromMicros` to convert time from the DV timestamp format.