## /bin/bash

# Select first executable found under this directory
EXECUTABLE=`find . -name run_aedat4_sample | head -n 1`

if [[-z $EXECUTABLE]]; then
  echo "Runtime executable not found, make sure to compile the project before running"
  exit 1
fi

$EXECUTABLE -a $PWD/dynamic_6dof.aedat4 -c src/uzh-mono.yaml
