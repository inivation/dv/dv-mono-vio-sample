# dv-mono-vio-sample

**THIS PROJECT IS NOT PRODUCTION READY, it is intended as a demonstrator of capabilities, for users to develop further according to their requirements.**

This repository serves as a sample visual-inertial odometry algorithm implementation for event cameras
using the `dv::processing` library as a frontend.

The sample implementation currently supports reading an `aedat4` file, which contains imu recorded
data and at least images or events (if both are present, it will run in combined mode using both input streams).

## Project structure

The code is organized in the following directories:
* thirdparty - directory containing external project code as submodules, the original okvis code and some helper libraries.
* okvis_modified - contains code fragments from okvis and modified to support dv::processing as input, subdirectories are in the same structure as original okvis code. Each modified directory contains a small readme file describing the applied changes.
* src - source directory for executables, which read dataset recording either from aedat4 file or RPG DAVIS format dataset. Custom dataset should prefer usage of aedat4 as it is much more efficient.

## Dependencies

List of dependencies:
* dv::processing (please install it according to its own documentation)
* glog
* SuiteSparse
* gcc and g++ at least version 10.0

Installation command (for ubuntu linux :
```bash
sudo apt install libgoogle-glog-dev libsuitesparse-dev gcc-10 g++10
```

## Compilation

Follow these steps:
1. Clone or extract this repository
2. Follow the standard cmake project build, execute commands from this repository root directory:
```
mkdir build
cd build
CC=gcc-10 CXX=g++-10 cmake -DBUILD_APPS=OFF -DCMAKE_BUILD_TYPE=Release ..
make
```

## Running the algorithm

Please follow the compilation procedure described above.

To run the sample algorithm, you need to provide two items: configuration file and dataset recording.
* Configuration file must contain camera calibration and OKVIS algorithm parameters in yaml format, please refer to `src/uzh-mono.yaml` file for an example.
* Dataset can be read from RPG DAVIS Event camera dataset format or you can provide aedat4 file containing DAVIS camera recording (frames, events, IMU).

### Running RPG DAVIS Event camera dataset

1. Download a dataset in zip format from the RPG DAVIS Repository (http://rpg.ifi.uzh.ch/davis_data.html). You can
dynamic_6dof as a sample:
```
wget http://rpg.ifi.uzh.ch/datasets/davis/dynamic_6dof.zip
```
2. Extract the archive, note the path to the dataset
```
unzip dynamic_6dof.zip
```
3. Run the executable that supports RPG dataset:
```
./build/src/run_rpg_sample -d /path/to/dynamic_6dof -c /this_repo/configs/uzh-mono.yaml
```

Preview windows should show up displaying estimated trajectory and feature tracks on images and motion compensated
frames.

### Running aedat4 file

A sample recordings and configuration files can be downloaded from [here](https://release.inivation.com/?prefix=datasets/vio-sample/).  
Run the executable that supports aedat4 files, e.g. using the office_loop recording:
```
./build/src/run_aedat4_sample -a /path/to/office_loop.aedat4 -c /this_repo/configs/office_loop.yaml
```
or using events only:
```
./build/src/run_events_only_aedat4 -a /path/to/office_loop.aedat4 -c /this_repo/configs/office_loop.yaml
```
