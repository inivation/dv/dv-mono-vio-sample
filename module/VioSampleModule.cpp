#include <dv-sdk/module.hpp>

#include <dv-processing/processing.hpp>
#include <dv-processing/visualization/colors.hpp>

#include <okvis/ThreadedKFVio.hpp>
#include <okvis/VioParametersReader.hpp>

namespace dvk = dv::kinematics;
namespace dvf = dv::features;
namespace dvv = dv::visualization;

using DepthQueue = boost::lockfree::spsc_queue<std::pair<int64_t, float>>;
using PoseQueue = boost::lockfree::spsc_queue<dvk::Transformationf>;
using RejectionQueue = boost::lockfree::spsc_queue<std::vector<int>>;
using CameraPtr = dv::camera::CameraGeometry::SharedPtr;
using TrackerType = dvf::EventCombinedLKTracker<dvk::MotionCompensator<>>;

class VioSampleModule : public dv::ModuleBase {
private:
  okvis::VioParameters parameters;

  PoseQueue poseQueue = PoseQueue(10000);
  RejectionQueue rejections = RejectionQueue(10000);
  DepthQueue depthQueue = DepthQueue(10000);

  std::unique_ptr<okvis::ThreadedKFVio> vio;

  dvf::FeatureTracks frameTracks;
  int64_t lastImuTimestamp = 0;
  int64_t lastTrackTimestamp = 0;
  int64_t imuTimeShift;
  int64_t imuTimeOverhead = 5000;
  int64_t lastPublishTime = -1;
  int64_t lastPoseTime = -1;
  double zeroSpeedRateThreshold = 30000.0;
  cv::Size sensorResolution;

  CameraPtr camera;
  std::unique_ptr<TrackerType> frameTracker;
  TrackerType::Result::ConstPtr lastTrackResult;

  std::deque<dv::Frame> frames;

  static CameraPtr fromIntrinsicVector(const Eigen::VectorXd &intrinsics,
                                       const cv::Size &resolution) {
    std::vector<float> distortion;

    for (Eigen::Index i = 4; i < intrinsics.size(); i++) {
      distortion.push_back(static_cast<float>(intrinsics(i)));
    }

    return std::make_shared<dv::camera::CameraGeometry>(
        distortion, intrinsics(0), intrinsics(1), intrinsics(2), intrinsics(3),
        resolution, dv::camera::DistortionModel::RadTan);
  }

public:
  static void initInputs(dv::InputDefinitionList &in) {
    in.addEventInput("events");
    in.addFrameInput("frames");
    in.addIMUInput("imu");
  }

  static void initOutputs(dv::OutputDefinitionList &out) {
    out.addFrameOutput("events");
    out.addFrameOutput("frames");
    out.addOutput("pose", dv::Pose::TableType::identifier);
  }

  static const char *initDescription() {
    return "Visual-inertial odometry sample.";
  }

  static void initTypes(std::vector<dv::Types::Type> &types) {
    types.push_back(dv::Types::makeTypeDefinition<dv::Pose, dv::Pose>(
        "Camera pose estimate"));
  }

  static void initConfigOptions(dv::RuntimeConfig &config) {
    config.add("configFile",
               dv::ConfigOption::fileOpenOption("OKVIS Config file", "yaml"));
    config.add("zeroSpeedThreshold",
               dv::ConfigOption::doubleOption(
                   "Coefficient of event resolution and event rate that is "
                   "used to assume no motion when there are few incoming "
                   "events from the sensor",
                   0.3, 0.0, 10.0));
    config.setPriorityOptions({"configFile"});
  }

  void applyConfiguration() {
    double zeroSpeedThreshold = config.getDouble("zeroSpeedThreshold");
    zeroSpeedRateThreshold = zeroSpeedThreshold * sensorResolution.area();
    frameTracker->setMinRateForIntermediateTracking(zeroSpeedRateThreshold);
  }

  void configUpdate() override { applyConfiguration(); }

  VioSampleModule() {
    okvis::VioParametersReader vio_parameters_reader(
        config.getString("configFile"));
    vio_parameters_reader.getParameters(parameters);

    imuTimeShift = static_cast<int64_t>(parameters.imu.shift * 1e+6);

    // Initialize the OKVIS VIO pipeline
    vio = std::make_unique<okvis::ThreadedKFVio>(parameters);
    vio->setBlocking(true);

    auto T_SC = parameters.nCameraSystem.T_SC(0);

    vio->setStateCallback([this, T_SC](
                              const okvis::Time &timestamp,
                              const okvis::kinematics::Transformation &T_WS) {
      if (vio->isInitialized()) {
        dvk::Transformationf transform(
            static_cast<int64_t>(timestamp.toSec() * 1e+6),
            (T_WS.T() * T_SC->T()).cast<float>());
        poseQueue.push(transform);

        rejections.push(vio->getTrackRejections(0));

        auto translation = transform.getTranslation();
        auto quaternion = transform.getQuaternion();

        dv::Pose output(
            transform.getTimestamp(),
            dv::Vec3f(translation.x(), translation.y(), translation.z()),
            dv::Quaternion(quaternion.w(), quaternion.x(), quaternion.y(),
                           quaternion.z()),
            "world", "camera");

        if (lastPublishTime < 0 || output.timestamp - lastPublishTime > 33000) {
          auto outputStream = outputs.getOutput<dv::Pose>("pose").data();
          *outputStream = output;
          outputStream.commit();
          lastPublishTime = output.timestamp;
        }
      }
    });

    // Depth is estimated by using the median depth of current landmarks
    // Queue them up, they will be used later on
    vio->setLandmarksCallback([this](const okvis::Time &timestamp,
                                     const okvis::MapPointVector &landmarks,
                                     const okvis::MapPointVector &_) {
      if (!landmarks.empty()) {
        std::vector<double> depths;
        depths.reserve(landmarks.size());
        for (const auto &landmark : landmarks) {
          // Distance of 1e+3 is used to denote landmarks that are
          // beyond triangulation distance
          if (landmark.distance < 999.0) {
            depths.emplace_back(landmark.distance);
          }
        }
        std::sort(depths.begin(), depths.end());
        double medianDepth = depths[depths.size() / 2];
        auto time_us = static_cast<int64_t>(timestamp.toSec() * 1e+6);
        depthQueue.push(
            std::make_pair(time_us, static_cast<float>(medianDepth)));
      }
    });

    // Parse the camera geometry
    const auto &cameraGeometry = parameters.nCameraSystem.cameraGeometry(0);

    // Sensor resolution
    sensorResolution =
        cv::Size(static_cast<int>(cameraGeometry->imageWidth()),
                 static_cast<int>(cameraGeometry->imageHeight()));

    // Retrieve the camera calibration
    Eigen::VectorXd intrinsics;
    cameraGeometry->getIntrinsics(intrinsics);
    camera = fromIntrinsicVector(intrinsics, sensorResolution);

    // Build the trackers
    dvf::ImageFeatureLKTracker::Config config;
    config.terminationEpsilon = 1e-2;
    config.numPyrLayers = parameters.optimization.detectionOctaves;

    auto fast = cv::FastFeatureDetector::create(
        static_cast<int>(parameters.optimization.detectionThreshold));
    auto detector = std::make_unique<dv::features::ImagePyrFeatureDetector>(
        camera->getResolution(), fast);
    frameTracker = TrackerType::MotionAwareTracker(
        camera, config, nullptr, nullptr, std::move(detector));
    frameTracker->setMaxTracks(
        static_cast<size_t>(parameters.optimization.maxNoKeypoints));
    frameTracker->setLookbackRejection(true);

    auto input = inputs.getEventInput("events");

    outputs.getOutput<dv::Pose>("pose").setup(input.getOriginDescription());
    outputs.getFrameOutput("frames").setup(inputs.getFrameInput("frames"));
    outputs.getFrameOutput("events").setup(input.sizeX(), input.sizeY(),
                                           input.getOriginDescription());

    applyConfiguration();
  }

  void runImu() {
    auto imu = inputs.getIMUInput("imu").data();
    if (imu.empty()) {
      // No new IMU data
      return;
    }

    for (const auto &singleImu : imu) {
      int64_t imuTime = singleImu.timestamp - imuTimeShift;
      if (imuTime > lastImuTimestamp) {
        vio->addImuMeasurement(okvis::Time::fromMicros(imuTime),
                               singleImu.getAccelerations().cast<double>(),
                               singleImu.getAngularVelocities().cast<double>());
        lastImuTimestamp = imuTime;
      }
    }
  }

  void runEvents() {
    auto events = inputs.getEventInput("events").data();
    if (events) {
      dv::EventStore store(events);

      double eventRate = store.rate();
      if (eventRate < zeroSpeedRateThreshold) {
        vio->setZeroSpeedPrior(true);
      } else {
        vio->setZeroSpeedPrior(false);
      }

      try {
        frameTracker->accept(store);
      } catch (...) {
        // Noop
      }
    }
  }

  void runFrames() {
    auto frame = inputs.getFrameInput("frames").data();
    if (frame) {
      frames.push_back(*frame.getBasePointer());
    }
  }

  bool isThereEnoughImuMeasurements(const int64_t timestamp) {
      dv::runtime_assert(parameters.imu.rate > 0, "IMU rate is zero");
      const int64_t period_us = 1'000'000 / parameters.imu.rate;
      auto start = okvis::Time::fromMicros(timestamp - (5 * period_us));
      auto stop = okvis::Time::fromMicros(timestamp + (5 * period_us));
      const auto measurements = vio->getImuMeasurments(start, stop);
      return measurements.size() > 5;
  }

  void runTracking() {
    if (frames.empty()) {
      return;
    }

    const auto &frame = frames.front();
    if ((lastImuTimestamp - frame.timestamp) >= imuTimeOverhead && isThereEnoughImuMeasurements(frame.timestamp)) {
      try {
        frameTracker->accept(frame);
      } catch (...) {
        frames.pop_front();
        return;
      }
      if (auto tracks = frameTracker->runTracking()) {
        frameTracks.accept(tracks);
        bool asKeyFrame = tracks->asKeyFrame;

        vio->addKeypoints(okvis::Time::fromMicros(tracks->timestamp), 0,
                          dv::data::fromTimedKeyPoints(tracks->keypoints), {},
                          cv::Mat(), &asKeyFrame);
        lastTrackTimestamp = tracks->timestamp;

        // Lastly - visualization, retrieve images, draw tracks and so on
        cv::Mat preview = frameTracks.visualize(frame.image);
        outputs.getFrameOutput("frames")
            << frame.timestamp << preview << dv::commit;

        auto intermediateFrames = frameTracker->getAccumulatedFrames();
        if (!intermediateFrames.empty()) {
          cv::Mat eventPreview;
          cv::cvtColor(intermediateFrames.back().pyramid.front(), eventPreview,
                       cv::COLOR_GRAY2BGR);
          const auto eventFeatures = frameTracker->getEventTrackPoints();
          for (const auto &point : eventFeatures.back()) {
            cv::drawMarker(eventPreview, point, dvv::colors::magenta,
                           cv::MARKER_SQUARE, 3);
          }
          outputs.getFrameOutput("events")
              << frame.timestamp << eventPreview << dv::commit;
        }
      }
      frames.pop_front();
    }
  }

  void run() {
    // Retrieve asynchronously computed poses and camera depths
    // These are required for motion compensation
    poseQueue.consume_all([this](const dvk::Transformationf &T_WC) {
      if (T_WC.getTimestamp() > lastPoseTime) {
        frameTracker->accept(T_WC);
        lastPoseTime = T_WC.getTimestamp();
      }
    });

    // Remove rejected tracks
    rejections.consume_all([this](const std::vector<int> &trackIds) {
      frameTracker->removeTracks(trackIds);
    });

    // Pass depth to the vio tracker
    depthQueue.consume_all([this](const std::pair<int64_t, float> &depth) {
      frameTracker->accept(dv::measurements::Depth(depth.first, depth.second));
    });


    runImu();
    runEvents();
    runFrames();
    runTracking();
  }
};

registerModuleClass(VioSampleModule);
