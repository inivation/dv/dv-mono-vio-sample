#include <dv-processing/data/utilities.hpp>
#include <dv-processing/features/event_feature_lk_tracker.hpp>
#include <dv-processing/features/feature_tracks.hpp>
#include <dv-processing/io/camera_capture.hpp>
#include <dv-processing/io/mono_camera_recording.hpp>
#include <dv-processing/kinematics/motion_compensator.hpp>
#include <dv-processing/visualization/pose_visualizer.hpp>

#include <CLI/CLI.hpp>
#include <boost/lockfree/spsc_queue.hpp>

#include <cstdlib>
#include <filesystem>
#include <okvis/ThreadedKFVio.hpp>
#include <okvis/VioParametersReader.hpp>

// Namespace aliases
namespace dvf = dv::features;
namespace dvk = dv::kinematics;
namespace dvv = dv::visualization;
namespace fs  = std::filesystem;

// Type aliases
using DepthQueue     = boost::lockfree::spsc_queue<std::pair<int64_t, float>>;
using PoseQueue      = boost::lockfree::spsc_queue<dvk::Transformationf>;
using RejectionQueue = boost::lockfree::spsc_queue<std::vector<int>>;
using CameraPtr      = dv::camera::CameraGeometry::SharedPtr;
using LandmarksQueue = boost::lockfree::spsc_queue<dv::LandmarksPacket>;

// Create dv camera geometry class from intrinsics vector
CameraPtr fromIntrinsicVector(const Eigen::VectorXd &intrinsics, const cv::Size &resolution);

int main(int ac, char **av) {
	using namespace std::chrono_literals;

	std::string datasetPath;
	std::string configPath;

	// Configuration parameters
	CLI::App app{"OKVIS Visual-inertial odometry on DAVIS data"};

	app.add_option("-a,--aedat4-dataset", datasetPath, "Path an aedat4 file containing DAVIS frames and events.")
		->required()
		->check(CLI::ExistingFile);
	app.add_option("-c,--config", configPath,
		   "Path to yaml config file containing camera calibration and "
		   "OKVIS algorithm parameters.")
		->required()
		->check(CLI::ExistingFile);
	try {
		app.parse(ac, av);
	}
	catch (const CLI::ParseError &e) {
		return app.exit(e);
	}

	if (fs::path(datasetPath).extension() != ".aedat4") {
		std::cerr << "The provided path [" << datasetPath << "] is not an aedat4 file, please check the path."
				  << std::endl;
		return EXIT_FAILURE;
	}

	if (fs::path(configPath).extension() != ".yaml") {
		std::cerr << "The provided configuration file path [" << configPath
				  << "] is not an '.yaml' file, please check the path." << std::endl;
		return EXIT_FAILURE;
	}

	// Read the configuration parameters from the yaml file
	okvis::VioParametersReader vio_parameters_reader(configPath);
	okvis::VioParameters parameters;
	vio_parameters_reader.getParameters(parameters);

	// Initialize the OKVIS VIO pipeline
	okvis::ThreadedKFVio vio(parameters);
	vio.setBlocking(true);

	// IMU->Camera transformation
	auto T_SC = parameters.nCameraSystem.T_SC(0);

	// Queue up the pose estimates from OKVIS pipeline, we will need them later on
	PoseQueue poseQueue(10000);
	RejectionQueue rejections(10000);
	vio.setStateCallback([&T_SC, &poseQueue, &vio, &rejections](
							 const okvis::Time &timestamp, const okvis::kinematics::Transformation &T_WS) {
		if (vio.isInitialized()) {
			dvk::Transformationf transform(
				static_cast<int64_t>(timestamp.toSec() * 1e+6), (T_WS.T() * T_SC->T()).cast<float>());
			poseQueue.push(transform);
		}

		// Track rejection
		const auto rejectedTracks = vio.getTrackRejections(0);
		if (!rejectedTracks.empty()) {
			rejections.push(rejectedTracks);
		}
	});

	// Depth is estimated by using the median depth of current landmarks
	// Queue them up, they will be used later on
	DepthQueue depthQueue(1000);
	LandmarksQueue landmarkQueue(1000);

	vio.setLandmarksCallback([&depthQueue, &landmarkQueue](const okvis::Time &timestamp,
								 const okvis::MapPointVector &landmarks, const okvis::MapPointVector &_) {
		if (!landmarks.empty()) {
			auto time_us = (static_cast<int64_t>(timestamp.sec) * 1000) + (static_cast<int64_t>(timestamp.nsec) / 1000);
			std::vector<double> depths;
			depths.reserve(landmarks.size());
			dv::LandmarksPacket landmarkPacket;
			if (!landmarks.empty()) {
				landmarkPacket.elements.reserve(landmarks.size());
				for (const auto &landmark : landmarks) {
					depths.emplace_back(landmark.distance);
					if (landmark.quality > 0.1) {
						Eigen::Vector4f point = landmark.point.cast<float>();
						// Normalize just in case
						point /= point(3);
						landmarkPacket.elements.emplace_back(dv::Landmark(dv::Point3f(point.x(), point.y(), point.z()),
							static_cast<int64_t>(landmark.id), time_us, {}, "", {}, {}));
					}
				}
				landmarkQueue.push(landmarkPacket);

				std::sort(depths.begin(), depths.end());
				double medianDepth = depths[depths.size() / 2];
				depthQueue.push(std::make_pair(time_us, static_cast<float>(medianDepth)));
			}
		}
	});

	// RPG format dataset reader
	auto reader = dv::io::MonoCameraRecording(datasetPath);

	if (!reader.isImuStreamAvailable()) {
		std::cerr << "The provided aedat4 file [" << datasetPath
				  << "] does not contain imu data stream, this is required for the "
					 "algorithm to run."
				  << std::endl;
		return EXIT_FAILURE;
	}
	if (!reader.isEventStreamAvailable()) {
		std::cerr << "The provided aedat4 file [" << datasetPath
				  << "] does not contain event data stream, this is required for "
					 "the algorithm to run."
				  << std::endl;
		return EXIT_FAILURE;
	}

	// Parse the camera geometry
	const auto &cameraGeometry = parameters.nCameraSystem.cameraGeometry(0);

	// Sensor resolution
	cv::Size sensorResolution(
		static_cast<int>(cameraGeometry->imageWidth()), static_cast<int>(cameraGeometry->imageHeight()));

	// Retrieve the camera calibration
	Eigen::VectorXd intrinsics;
	cameraGeometry->getIntrinsics(intrinsics);

	// Custom function to build dv::processing camera geometry class
	auto camera = fromIntrinsicVector(intrinsics, sensorResolution);

	// Build the trackers
	dvf::ImageFeatureLKTracker::Config config;
	config.terminationEpsilon = 1e-2;
	config.numPyrLayers       = parameters.optimization.detectionOctaves;

	// Event accumulated frames require a higher FAST feature threshold
	// to reduce the amount of false positive corner detections
	auto fast     = cv::FastFeatureDetector::create(100);
	auto detector = std::make_unique<dv::features::ImagePyrFeatureDetector>(camera->getResolution(), fast);

	auto frameTracker = dvf::EventFeatureLKTracker<dv::kinematics::MotionCompensator<>>::MotionAwareTracker(camera,
		config, nullptr, nullptr, std::move(detector), std::make_unique<dv::features::FeatureCountRedetection>(0.9));
	frameTracker->setMaxTracks(static_cast<size_t>(parameters.optimization.maxNoKeypoints));
	// Tracking speed
	frameTracker->setFramerate(parameters.sensors_information.cameraRate);

	int64_t backendFeedInterval = 1000000 / parameters.sensors_information.cameraRate;

	// Track history and visualization windows
	dvf::FeatureTracks frameTracks;
	frameTracks.setHistoryDuration(100ms);
	cv::namedWindow("Preview", cv::WINDOW_AUTOSIZE);
	cv::namedWindow("Trajectory", cv::WINDOW_AUTOSIZE);

	// Amount of time in microseconds, the imu will be 5ms in front of the frame
	int64_t imuOverheadDuration = 5000;

	// Camera pose estimation visualization
	dvv::PoseVisualizer visualizer(1000000);
	visualizer.setViewMode(dvv::PoseVisualizer::Mode::VIEW_XY);
	visualizer.setGridPlane(dvv::PoseVisualizer::GridPlane::PLANE_XY);

	// Sequential read of images
	int64_t lastImuTimestamp   = 0;
	int64_t lastTrackTimestamp = 0;
	while (const auto events = reader.getNextEventBatch()) {
		if (lastImuTimestamp < events->getHighestTime() + imuOverheadDuration) {
			while (const auto imuData = reader.getNextImuBatch()) {
				const double earthG         = parameters.imu.g;
				static const double deg2rad = std::numbers::pi / 180.;
				for (const auto &imu : *imuData) {
					if (lastImuTimestamp == imu.timestamp) {
						continue;
					}
					vio.addImuMeasurement(okvis::Time::fromMicros(imu.timestamp),
						Eigen::Vector3d(static_cast<double>(imu.accelerometerX) * earthG,
							static_cast<double>(imu.accelerometerY) * earthG,
							static_cast<double>(imu.accelerometerZ) * earthG),
						Eigen::Vector3d(static_cast<double>(imu.gyroscopeX) * deg2rad,
							static_cast<double>(imu.gyroscopeY) * deg2rad,
							static_cast<double>(imu.gyroscopeZ) * deg2rad));
					lastImuTimestamp = imu.timestamp;
				}
				if (lastImuTimestamp >= events->getHighestTime() + imuOverheadDuration) {
					break;
				}
			}
		}

		// Retrieve asynchronously computed poses
		// These are required for motion compensation
		poseQueue.consume_all([&visualizer, &frameTracker](const dvk::Transformationf &T_WC) {
			visualizer.accept(T_WC);
			frameTracker->accept(T_WC);
		});

		depthQueue.consume_all([&frameTracker](const std::pair<int64_t, float> &depth) {
			dv::measurements::Depth depthMeasurement(depth.first, 0.5f);
			frameTracker->accept(depthMeasurement);
		});
		landmarkQueue.consume_all([&visualizer](const dv::LandmarksPacket &landmarks) {
			visualizer.accept(landmarks);
		});

		// Remove rejected tracks
		rejections.consume_all([&frameTracker](const std::vector<int> &trackIds) {
			frameTracker->removeTracks(trackIds);
		});

		// Track on an frame and estimate pose
		frameTracker->accept(*events);
		while (auto tracks = frameTracker->runTracking()) {
			frameTracks.accept(tracks);

			bool asKeyFrame = tracks->asKeyFrame;

			// Feed the backend at the camera_rate from the config file
			if (!(asKeyFrame || (tracks->timestamp - lastTrackTimestamp > backendFeedInterval))) {
				continue;
			}
			// Push in the measurements
			vio.addKeypoints(okvis::Time::fromMicros(tracks->timestamp), 0,
				dv::data::fromTimedKeyPoints(tracks->keypoints), {}, cv::Mat(), &asKeyFrame);
			lastTrackTimestamp = tracks->timestamp;
			// Lastly - visualization, retrieve images, draw tracks and so on
			cv::imshow("Preview", frameTracks.visualize(frameTracker->getAccumulatedFrame()));
			cv::waitKey(1);

			cv::imshow("Trajectory", visualizer.generateFrame().image);
			cv::waitKey(1);
		}
	}

	return EXIT_SUCCESS;
}

CameraPtr fromIntrinsicVector(const Eigen::VectorXd &intrinsics, const cv::Size &resolution) {
	std::vector<float> distortion;

	for (Eigen::Index i = 4; i < intrinsics.size(); i++) {
		distortion.push_back(static_cast<float>(intrinsics(i)));
	}

	return std::make_shared<dv::camera::CameraGeometry>(distortion, intrinsics(0), intrinsics(1), intrinsics(2),
		intrinsics(3), resolution, dv::camera::DistortionModel::RadTan);
}
