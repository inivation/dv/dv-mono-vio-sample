#include "KeyPointInterpolation.h"

#include <iostream>
#include <utility>

void slam::KeyPointInterpolation::addKeyPoints(int64_t timestamp, const std::vector<cv::KeyPoint> &points) {
	buffer.push_back(KeyPointMeasurement(timestamp, points));
}

std::optional<std::vector<cv::KeyPoint>> slam::KeyPointInterpolation::interpolate(int64_t atTimestamp) const {
	if (atTimestamp < buffer.front().timestamp || atTimestamp > buffer.back().timestamp || buffer.size() < 2) {
		std::cout << "* INTERPOLATOR: Requested interpolation at " << (atTimestamp * 1e-6)
				  << ", but tracks are only available within [" << (buffer.front().timestamp * 1e-6) << "; "
				  << (buffer.back().timestamp * 1e-6) << "]" << std::endl;

		// extrapolation is not handled
		return std::nullopt;
	}

	// Get distance from timestamp
	int64_t fullDistance = buffer.back().timestamp - buffer.front().timestamp;
	if (fullDistance == 0) {
		// An exceptional case when both items have the same timestamp, no interpolation can be performed
		return std::nullopt;
	}
	int64_t distanceToTarget = fullDistance - (buffer.back().timestamp - atTimestamp);
	float coeff              = static_cast<float>(distanceToTarget) / static_cast<float>(fullDistance);
	std::vector<cv::KeyPoint> interpolatedPoints;
	for (const auto &kpt : buffer.back().kpts) {
		// Match a keypoint from previous state
		auto matchKpt
			= std::find_if(buffer.front().kpts.begin(), buffer.front().kpts.end(), [&kpt](const cv::KeyPoint &kp) {
				  return kpt.class_id == kp.class_id;
			  });
		if (matchKpt != buffer.front().kpts.end()) {
			// Linear interpolation
			interpolatedPoints.emplace_back(matchKpt->pt.x + ((kpt.pt.x - matchKpt->pt.x) * coeff),
				matchKpt->pt.y + ((kpt.pt.y - matchKpt->pt.y) * coeff), kpt.size, kpt.angle, kpt.response, kpt.octave,
				kpt.class_id);
		}
	}
	return interpolatedPoints;
}

slam::KeyPointInterpolation::KeyPointMeasurement::KeyPointMeasurement(
	int64_t timestamp, std::vector<cv::KeyPoint> kpts) :
	timestamp(timestamp), kpts(std::move(kpts)) {
}
