#pragma once

#include <dv-processing/core/core.hpp>

#include <boost/circular_buffer.hpp>
#include <opencv2/core/core.hpp>

#include <optional>

namespace slam {

/**
 * KeyPoint interpolation class. It keeps two copies of latest frames and performs interpolation between them.
 */
class KeyPointInterpolation {
	struct KeyPointMeasurement {
		int64_t timestamp;
		std::vector<cv::KeyPoint> kpts;

		explicit KeyPointMeasurement(int64_t timestamp, std::vector<cv::KeyPoint> kpts);
	};

public:
	/**
	 * Add next keypoints to the interpolator
	 * @param timestamp     Timestamp
	 * @param points        Keypoints
	 */
	void addKeyPoints(int64_t timestamp, const std::vector<cv::KeyPoint> &points);

	/**
	 * Request to interpolate between buffered keypoints.
	 * @param atTimestamp   Time to interpolate at
	 * @return              Interpolated keypoints or std::nullopt in case the timestamp requires extrapolation
	 */
	[[nodiscard]] std::optional<std::vector<cv::KeyPoint>> interpolate(int64_t atTimestamp) const;

protected:
	boost::circular_buffer<KeyPointMeasurement> buffer = boost::circular_buffer<KeyPointMeasurement>(100);
};

} // namespace slam
