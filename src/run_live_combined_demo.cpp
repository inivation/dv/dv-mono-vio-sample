#include <dv-processing/data/utilities.hpp>
#include <dv-processing/features/event_combined_lk_tracker.hpp>
#include <dv-processing/features/feature_tracks.hpp>
#include <dv-processing/io/camera_capture.hpp>
#include <dv-processing/kinematics/motion_compensator.hpp>
#include <dv-processing/noise/background_activity_noise_filter.hpp>
#include <dv-processing/visualization/pose_visualizer.hpp>

#include <CLI/CLI.hpp>
#include <boost/lockfree/spsc_queue.hpp>

#include <csignal>
#include <cstdlib>
#include <filesystem>
#include <okvis/ThreadedKFVio.hpp>
#include <okvis/VioParametersReader.hpp>

// Namespace aliases
namespace dvf = dv::features;
namespace dvk = dv::kinematics;
namespace dvv = dv::visualization;
namespace fs  = std::filesystem;

// Type aliases
using DepthQueue     = boost::lockfree::spsc_queue<std::pair<int64_t, float>>;
using PoseQueue      = boost::lockfree::spsc_queue<dvk::Transformationf>;
using RejectionQueue = boost::lockfree::spsc_queue<std::vector<int>>;
using LandmarksQueue = boost::lockfree::spsc_queue<dv::LandmarksPacket>;
using CameraPtr      = dv::camera::CameraGeometry::SharedPtr;

// Create dv camera geometry class from intrinsics vector
CameraPtr fromIntrinsicVector(const Eigen::VectorXd &intrinsics, const cv::Size &resolution);

static std::atomic<bool> globalShutdown(false);

static void globalShutdownSignalHandler(int signal) {
	globalShutdown.store(true);
}

int main(int ac, char **av) {
	using namespace std::chrono_literals;

	std::string configPath;

	std::signal(SIGTERM, &globalShutdownSignalHandler);
	std::signal(SIGINT, &globalShutdownSignalHandler);

	// Configuration parameters
	CLI::App app{"OKVIS Visual-inertial odometry on DAVIS data"};

	app.add_option("-c,--config", configPath,
		   "Path to yaml config file containing camera calibration and "
		   "OKVIS algorithm parameters.")
		->required()
		->check(CLI::ExistingFile);
	try {
		app.parse(ac, av);
	}
	catch (const CLI::ParseError &e) {
		return app.exit(e);
	}

	if (fs::path(configPath).extension() != ".yaml") {
		std::cerr << "The provided configuration file path [" << configPath
				  << "] is not an '.yaml' file, please check the path." << std::endl;
		return EXIT_FAILURE;
	}

	// Read the configuration parameters from the yaml file
	okvis::VioParametersReader vio_parameters_reader(configPath);
	okvis::VioParameters parameters;
	vio_parameters_reader.getParameters(parameters);

	// Initialize the OKVIS VIO pipeline
	okvis::ThreadedKFVio vio(parameters);
	vio.setBlocking(true);

	// IMU->Camera transformation
	auto T_SC = parameters.nCameraSystem.T_SC(0);

	// Queue up the pose estimates from OKVIS pipeline, we will need them later on
	PoseQueue poseQueue(10000);
	RejectionQueue rejections(10000);
	vio.setStateCallback([&T_SC, &poseQueue, &vio, &rejections](
							 const okvis::Time &timestamp, const okvis::kinematics::Transformation &T_WS) {
		if (vio.isInitialized()) {
			dvk::Transformationf transform(
				static_cast<int64_t>(timestamp.toSec() * 1e+6), (T_WS.T() * T_SC->T()).cast<float>());
			poseQueue.push(transform);
		}

		rejections.push(vio.getTrackRejections(0));
	});

	DepthQueue depthQueue(1000);
	LandmarksQueue landmarkQueue(10000);
	vio.setLandmarksCallback([&depthQueue, &landmarkQueue](const okvis::Time &timestamp,
								 const okvis::MapPointVector &landmarks, const okvis::MapPointVector &_) {
		if (!landmarks.empty()) {
			auto time_us = (static_cast<int64_t>(timestamp.sec) * 1000) + (static_cast<int64_t>(timestamp.nsec) / 1000);
			std::vector<double> depths;
			depths.reserve(landmarks.size());
			dv::LandmarksPacket landmarkPacket;
			if (!landmarks.empty()) {
				landmarkPacket.elements.reserve(landmarks.size());
				for (const auto &landmark : landmarks) {
					depths.emplace_back(landmark.distance);
					if (landmark.quality > 0.1) {
						Eigen::Vector4f point = landmark.point.cast<float>();
						// Normalize just in case
						point /= point(3);
						landmarkPacket.elements.emplace_back(dv::Landmark(dv::Point3f(point.x(), point.y(), point.z()),
							static_cast<int64_t>(landmark.id), time_us, {}, "", {}, {}));
					}
				}
				landmarkQueue.push(landmarkPacket);

				std::sort(depths.begin(), depths.end());
				double medianDepth = depths[depths.size() / 2];
				depthQueue.push(std::make_pair(time_us, static_cast<float>(medianDepth)));
			}
		}
	});

	// Direct camera capture
	dv::io::CameraCapture reader;

	// Parse the camera geometry
	const auto &cameraGeometry = parameters.nCameraSystem.cameraGeometry(0);

	// Sensor resolution
	cv::Size sensorResolution(
		static_cast<int>(cameraGeometry->imageWidth()), static_cast<int>(cameraGeometry->imageHeight()));

	// Retrieve the camera calibration
	Eigen::VectorXd intrinsics;
	cameraGeometry->getIntrinsics(intrinsics);

	// Custom function to build dv::processing camera geometry class
	auto camera = fromIntrinsicVector(intrinsics, sensorResolution);

	// Build the trackers
	dvf::ImageFeatureLKTracker::Config config;
	config.terminationEpsilon = 1e-2;
	config.numPyrLayers       = parameters.optimization.detectionOctaves;

	auto fast         = cv::FastFeatureDetector::create(static_cast<int>(parameters.optimization.detectionThreshold));
	auto detector     = std::make_unique<dv::features::ImagePyrFeatureDetector>(camera->getResolution(), fast);
	auto frameTracker = dvf::EventCombinedLKTracker<dvk::MotionCompensator<>>::MotionAwareTracker(
		camera, config, nullptr, nullptr, std::move(detector), std::make_unique<dvf::FeatureCountRedetection>(0.75));
	frameTracker->setMaxTracks(static_cast<size_t>(parameters.optimization.maxNoKeypoints));
	// Tracking speed

	int64_t backendFeedInterval = 1000000 / parameters.sensors_information.cameraRate;

	// Track history and visualization windows
	dvf::FeatureTracks frameTracks;
	frameTracks.setHistoryDuration(100ms);
	cv::namedWindow("Preview", cv::WINDOW_AUTOSIZE);
	cv::namedWindow("Trajectory", cv::WINDOW_AUTOSIZE);

	cv::Mat preview(sensorResolution.height, sensorResolution.width * 2, CV_8UC3);
	cv::Mat framePreview = preview(cv::Rect(0, 0, sensorResolution.width, sensorResolution.height));
	cv::Mat eventPreview
		= preview(cv::Rect(sensorResolution.width, 0, sensorResolution.width, sensorResolution.height));

	// Amount of time in microseconds, the imu will be 5ms in front of the frame
	int64_t imuOverheadDuration = 5000;

	// Camera pose estimation visualization
	dvv::PoseVisualizer visualizer(1000000);
	visualizer.setViewMode(dvv::PoseVisualizer::Mode::VIEW_YZ);
	visualizer.setGridPlane(dvv::PoseVisualizer::GridPlane::PLANE_YZ);

	// Sequential read of images
	int64_t lastImuTimestamp   = 0;
	int64_t lastTrackTimestamp = 0;

	// 10 Seconds worth of imu timestamp history
	boost::circular_buffer<int64_t> imuTimestamps(10 * parameters.imu.rate);
	auto imuShift = static_cast<int64_t>(parameters.imu.shift * 1e+6);
	dvf::EventCombinedLKTracker<dvk::MotionCompensator<>>::Result::ConstPtr lastTrackResult;

	reader.setDavisExposureDuration(15ms);

	dv::noise::BackgroundActivityNoiseFilter noiseFilter(camera->getResolution(), dv::Duration(500));

	while (!globalShutdown) {
		const auto events = reader.getNextEventBatch();
		if (!events.has_value()) {
			std::this_thread::sleep_for(10us);
			continue;
		}
		if (lastImuTimestamp < events->getHighestTime() + imuOverheadDuration) {
			while (true) {
				if (const auto imuData = reader.getNextImuBatch(); imuData.has_value()) {
					const double earthG         = parameters.imu.g;
					static const double deg2rad = std::numbers::pi / 180.;
					for (const auto &imu : *imuData) {
						lastImuTimestamp = imu.timestamp - imuShift;
						imuTimestamps.push_back(lastImuTimestamp);
						vio.addImuMeasurement(okvis::Time::fromMicros(imu.timestamp),
							Eigen::Vector3d(static_cast<double>(imu.accelerometerX) * earthG,
								static_cast<double>(imu.accelerometerY) * earthG,
								static_cast<double>(imu.accelerometerZ) * earthG),
							Eigen::Vector3d(static_cast<double>(imu.gyroscopeX) * deg2rad,
								static_cast<double>(imu.gyroscopeY) * deg2rad,
								static_cast<double>(imu.gyroscopeZ) * deg2rad));
					}
					if (lastImuTimestamp >= events->getHighestTime() + imuOverheadDuration) {
						break;
					}
				}
				std::this_thread::sleep_for(10us);
			}
		}

		// Retrieve asynchronously computed poses and camera depths
		// These are required for motion compensation
		poseQueue.consume_all([&visualizer, &frameTracker](const dvk::Transformationf &T_WC) {
			visualizer.accept(T_WC);
			frameTracker->accept(T_WC);
		});

		// Remove rejected tracks
		rejections.consume_all([&frameTracker](const std::vector<int> &trackIds) {
			frameTracker->removeTracks(trackIds);
		});
		landmarkQueue.consume_all([&visualizer](const dv::LandmarksPacket &landmarks) {
			visualizer.accept(landmarks);
		});

		// Track on an frame and estimate pose
		frameTracker->accept(*events);

		const auto frame = reader.getNextFrame();
		if (!frame.has_value()) {
			continue;
		}
		frameTracker->accept(*frame);
		if (auto tracks = frameTracker->runTracking()) {
			float eventRate = events->size() * (1e+6f / static_cast<float>(events->duration().count()));
			frameTracks.accept(tracks);

			int64_t lowTime  = tracks->timestamp - imuOverheadDuration;
			int64_t highTime = tracks->timestamp + imuOverheadDuration;
			// Half of duration
			auto expectedImuMeasurementCount = static_cast<size_t>(
				static_cast<float>(parameters.imu.rate) * (static_cast<float>(imuOverheadDuration) * 1e-6));
			size_t numImuMeasurements
				= std::distance(std::lower_bound(imuTimestamps.begin(), imuTimestamps.end(), lowTime),
					std::lower_bound(imuTimestamps.begin(), imuTimestamps.end(), highTime));
			if (numImuMeasurements <= expectedImuMeasurementCount) {
				std::cout << "Warning: a gap in IMU measurements!" << std::endl;
				continue;
			}

			static bool initializing = true;

			if (initializing) {
				if (tracks->asKeyFrame) {
					lastTrackResult = tracks;
					continue;
				}
				else {
					bool yes = true;
					vio.addKeypoints(okvis::Time::fromMicros(lastTrackResult->timestamp), 0,
						dv::data::fromTimedKeyPoints(lastTrackResult->keypoints), {}, cv::Mat(), &yes);
					initializing = false;
				}
			}

			bool asKeyFrame = tracks->asKeyFrame;

			// Feed the backend at the camera_rate from the config file
			if (!(asKeyFrame || (tracks->timestamp - lastTrackTimestamp > backendFeedInterval))) {
				continue;
			}

			vio.addKeypoints(okvis::Time::fromMicros(tracks->timestamp), 0,
				dv::data::fromTimedKeyPoints(tracks->keypoints), {}, cv::Mat(), &asKeyFrame);
			lastTrackTimestamp = tracks->timestamp;

			// Lastly - visualization, retrieve images, draw tracks and so on
			frameTracks.visualize(frame->image).copyTo(framePreview);
			auto intermediateFrames = frameTracker->getAccumulatedFrames();
			if (!intermediateFrames.empty()) {
				cv::cvtColor(intermediateFrames.back().pyramid.front(), eventPreview, cv::COLOR_GRAY2BGR);
				const auto eventFeatures = frameTracker->getEventTrackPoints();
				for (const auto &point : eventFeatures.back()) {
					cv::drawMarker(eventPreview, point, dvv::colors::magenta, cv::MARKER_SQUARE, 3);
				}
			}
			cv::imshow("Preview", preview);
			cv::imshow("Trajectory", visualizer.generateFrame().image);

			// Exit if ESC key is pressed
			if (cv::waitKey(2) == 27) {
				globalShutdown = true;
			}
		}
	}

	return EXIT_SUCCESS;
}

CameraPtr fromIntrinsicVector(const Eigen::VectorXd &intrinsics, const cv::Size &resolution) {
	std::vector<float> distortion;

	for (Eigen::Index i = 4; i < intrinsics.size(); i++) {
		distortion.push_back(static_cast<float>(intrinsics(i)));
	}

	return std::make_shared<dv::camera::CameraGeometry>(distortion, intrinsics(0), intrinsics(1), intrinsics(2),
		intrinsics(3), resolution, dv::camera::DistortionModel::RadTan);
}
