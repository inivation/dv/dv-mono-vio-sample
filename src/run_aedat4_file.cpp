#include <dv-processing/data/utilities.hpp>
#include <dv-processing/features/event_combined_lk_tracker.hpp>
#include <dv-processing/features/feature_tracks.hpp>
#include <dv-processing/features/image_feature_lk_tracker.hpp>
#include <dv-processing/io/mono_camera_recording.hpp>
#include <dv-processing/kinematics/motion_compensator.hpp>
#include <dv-processing/visualization/pose_visualizer.hpp>

#include <CLI/CLI.hpp>
#include <boost/lockfree/spsc_queue.hpp>

#include <cstdlib>
#include <filesystem>
#include <okvis/ThreadedKFVio.hpp>
#include <okvis/VioParametersReader.hpp>

// Namespace aliases
namespace dvf = dv::features;
namespace dvk = dv::kinematics;
namespace dvv = dv::visualization;
namespace fs  = std::filesystem;

// Type aliases
using DepthQueue     = boost::lockfree::spsc_queue<std::pair<int64_t, float>>;
using PoseQueue      = boost::lockfree::spsc_queue<dvk::Transformationf>;
using CameraPtr      = dv::camera::CameraGeometry::SharedPtr;
using RejectionQueue = boost::lockfree::spsc_queue<std::vector<int>>;
using LandmarksQueue = boost::lockfree::spsc_queue<dv::LandmarksPacket>;

// Create dv camera geometry class from intrinsics vector
CameraPtr fromIntrinsicVector(const Eigen::VectorXd &intrinsics, const cv::Size &resolution);

int main(int ac, char **av) {
	std::string datasetPath;
	std::string configPath;

	// Configuration parameters
	CLI::App app{"OKVIS Visual-inertial odometry on DAVIS data"};

	app.add_option("-a,--aedat4-dataset", datasetPath, "Path an aedat4 file containing DAVIS frames and events.")
		->required()
		->check(CLI::ExistingFile);
	app.add_option("-c,--config", configPath,
		   "Path to yaml config file containing camera calibration and "
		   "OKVIS algorithm parameters.")
		->required()
		->check(CLI::ExistingFile);
	try {
		app.parse(ac, av);
	}
	catch (const CLI::ParseError &e) {
		return app.exit(e);
	}

	if (fs::path(datasetPath).extension() != ".aedat4") {
		std::cerr << "The provided path [" << datasetPath << "] is not an aedat4 file, please check the path."
				  << std::endl;
		return EXIT_FAILURE;
	}

	if (fs::path(configPath).extension() != ".yaml") {
		std::cerr << "The provided configuration file path [" << configPath
				  << "] is not an '.yaml' file, please check the path." << std::endl;
		return EXIT_FAILURE;
	}

	// Read the configuration parameters from the yaml file
	okvis::VioParametersReader vio_parameters_reader(configPath);
	okvis::VioParameters parameters;
	vio_parameters_reader.getParameters(parameters);

	// Initialize the OKVIS VIO pipeline
	okvis::ThreadedKFVio vio(parameters);
	vio.setBlocking(true);

	// IMU->Camera transformation
	auto T_SC = parameters.nCameraSystem.T_SC(0);

	// Queue up the pose estimates from OKVIS pipeline, we will need them later on
	PoseQueue poseQueue(10000);
	RejectionQueue rejections(10000);
	vio.setStateCallback([&vio, &T_SC, &poseQueue, &rejections](
							 const okvis::Time &timestamp, const okvis::kinematics::Transformation &T_WS) {
		if (vio.isInitialized()) {
			dvk::Transformationf transform(
				static_cast<int64_t>(timestamp.toSec() * 1e+6), (T_WS.T() * T_SC->T()).cast<float>());
			poseQueue.push(transform);
		}
		// Track rejection
		const auto rejectedTracks = vio.getTrackRejections(0);
		if (!rejectedTracks.empty()) {
			rejections.push(rejectedTracks);
		}
	});

	// Depth is estimated by using the median depth of current landmarks
	// Queue them up, they will be used later on
	DepthQueue depthQueue(1000);
	LandmarksQueue landmarkQueue(1000);

	vio.setLandmarksCallback([&depthQueue, &landmarkQueue](const okvis::Time &timestamp,
								 const okvis::MapPointVector &landmarks, const okvis::MapPointVector &_) {
		if (!landmarks.empty()) {
			auto time_us = (static_cast<int64_t>(timestamp.sec) * 1000) + (static_cast<int64_t>(timestamp.nsec) / 1000);
			std::vector<double> depths;
			depths.reserve(landmarks.size());
			dv::LandmarksPacket landmarkPacket;
			if (!landmarks.empty()) {
				landmarkPacket.elements.reserve(landmarks.size());
				for (const auto &landmark : landmarks) {
					depths.emplace_back(landmark.distance);
					if (landmark.quality > 0.1) {
						Eigen::Vector4f point = landmark.point.cast<float>();
						// Normalize just in case
						point /= point(3);
						landmarkPacket.elements.emplace_back(dv::Landmark(dv::Point3f(point.x(), point.y(), point.z()),
							static_cast<int64_t>(landmark.id), time_us, {}, "", {}, {}));
					}
				}
				landmarkQueue.push(landmarkPacket);

				std::sort(depths.begin(), depths.end());
				double medianDepth = depths[depths.size() / 2];
				depthQueue.push(std::make_pair(time_us, static_cast<float>(medianDepth)));
			}
		}
	});

	// RPG format dataset reader
	auto reader = dv::io::MonoCameraRecording(datasetPath);

	if (!reader.isFrameStreamAvailable()) {
		std::cerr << "The provided aedat4 file [" << datasetPath
				  << "] does not contain image frame stream, this is required for the "
					 "algorithm to run."
				  << std::endl;
		return EXIT_FAILURE;
	}
	if (!reader.isImuStreamAvailable()) {
		std::cerr << "The provided aedat4 file [" << datasetPath
				  << "] does not contain imu data stream, this is required for the "
					 "algorithm to run."
				  << std::endl;
		return EXIT_FAILURE;
	}
	if (!reader.isEventStreamAvailable()) {
		std::cerr << "The provided aedat4 file [" << datasetPath
				  << "] does not contain event data stream, this is required for "
					 "the algorithm to run."
				  << std::endl;
		return EXIT_FAILURE;
	}

	// Parse the camera geometry
	const auto &cameraGeometry = parameters.nCameraSystem.cameraGeometry(0);

	// Sensor resolution
	cv::Size sensorResolution(
		static_cast<int>(cameraGeometry->imageWidth()), static_cast<int>(cameraGeometry->imageHeight()));

	// Retrieve the camera calibration
	Eigen::VectorXd intrinsics;
	cameraGeometry->getIntrinsics(intrinsics);

	// Custom function to build dv::processing camera geometry class
	auto camera = fromIntrinsicVector(intrinsics, sensorResolution);

	// Build the trackers
	dvf::ImageFeatureLKTracker::Config config;
	config.numPyrLayers        = parameters.optimization.detectionOctaves;
	config.maskedFeatureDetect = true;
	config.terminationEpsilon  = 0.01;

	auto detector = std::make_unique<dvf::ImagePyrFeatureDetector>(camera->getResolution(),
		cv::FastFeatureDetector::create(static_cast<int>(parameters.optimization.detectionThreshold)));

	auto frameTracker = dvf::EventCombinedLKTracker<dvk::MotionCompensator<>>::MotionAwareTracker(
		camera, config, nullptr, nullptr, std::move(detector), std::make_unique<dvf::FeatureCountRedetection>(0.75));
	// This can be increased
	frameTracker->setMaxTracks(static_cast<size_t>(parameters.optimization.maxNoKeypoints));
	frameTracker->setNumIntermediateFrames(5);
	frameTracker->setLookbackRejection(true);

	// Track history and visualization windows
	dvf::FeatureTracks frameTracks;
	dvf::FeatureTracks eventTracks;
	cv::namedWindow("Preview", cv::WINDOW_NORMAL);
	cv::namedWindow("Trajectory", cv::WINDOW_AUTOSIZE);

	// Preview image, horizontal concat for image and event motion-compensated
	// frames
	cv::Mat preview(sensorResolution.height, sensorResolution.width * 2, CV_8UC3);
	cv::Mat framePreview = preview(cv::Rect(0, 0, sensorResolution.width, sensorResolution.height));
	cv::Mat eventPreview
		= preview(cv::Rect(sensorResolution.width, 0, sensorResolution.width, sensorResolution.height));

	// Amount of time in microseconds, the imu will be 5ms in front of the frame
	int64_t imuOverheadDuration = 5000;

	// Camera pose estimation visualization
	dvv::PoseVisualizer visualizer(1000000);
	visualizer.setViewMode(dvv::PoseVisualizer::Mode::VIEW_XY);
	visualizer.setGridPlane(dvv::PoseVisualizer::GridPlane::PLANE_XY);

	dvf::EventCombinedLKTracker<dvk::MotionCompensator<>>::Result::ConstPtr lastTrackResult;
	// Sequential read of images
	int64_t lastImuTimestamp = 0;
	auto imuShift            = static_cast<int64_t>(parameters.imu.shift * 1e+6);
	while (const auto frame = reader.getNextFrame()) {
		while (const auto imuData = reader.getNextImuBatch()) {
			const double earthG         = parameters.imu.g;
			static const double deg2rad = std::numbers::pi / 180.;
			for (const auto &imu : *imuData) {
				lastImuTimestamp = imu.timestamp - imuShift;
				vio.addImuMeasurement(okvis::Time::fromMicros(lastImuTimestamp),
					Eigen::Vector3d(static_cast<double>(imu.accelerometerX) * earthG,
						static_cast<double>(imu.accelerometerY) * earthG,
						static_cast<double>(imu.accelerometerZ) * earthG),
					Eigen::Vector3d(static_cast<double>(imu.gyroscopeX) * deg2rad,
						static_cast<double>(imu.gyroscopeY) * deg2rad, static_cast<double>(imu.gyroscopeZ) * deg2rad));
			}
			if (lastImuTimestamp >= frame->timestamp + imuOverheadDuration) {
				break;
			}
		}

		// Retrieve asynchronously computed poses and camera depths
		// These are required for motion compensation
		poseQueue.consume_all([&frameTracker, &visualizer](const dvk::Transformationf &T_WC) {
			frameTracker->accept(T_WC);
			visualizer.accept(T_WC);
		});
		depthQueue.consume_all([&frameTracker](const std::pair<int64_t, float> &depth) {
			dv::measurements::Depth depthMeasurement(depth.first, depth.second);
			frameTracker->accept(depthMeasurement);
		});

		landmarkQueue.consume_all([&visualizer](const dv::LandmarksPacket &landmarks) {
			visualizer.accept(landmarks);
		});

		// Remove rejected tracks
		rejections.consume_all([&frameTracker](const std::vector<int> &trackIds) {
			frameTracker->removeTracks(trackIds);
		});

		// Track on an frame and estimate pose
		frameTracker->accept(*frame);
		// Read events until the frame timestamp, events are used for intermediate
		// frame generation using motion compensation, it uses these
		// frame to predict locations of the tracks on frame frame

		while (const auto events = reader.getNextEventBatch()) {
			frameTracker->accept(*events);
			if (events->getHighestTime() >= frame->timestamp) {
				break;
			}
		}
		if (auto tracks = frameTracker->runTracking(); tracks != nullptr) {
			frameTracks.accept(tracks);

			static bool initializing = true;

			if (initializing) {
				if (tracks->asKeyFrame) {
					lastTrackResult = tracks;
					continue;
				}
				else {
					bool yes = true;
					vio.addKeypoints(okvis::Time::fromMicros(lastTrackResult->timestamp), 0,
						dv::data::fromTimedKeyPoints(lastTrackResult->keypoints), {}, cv::Mat(), &yes);
					initializing = false;
				}
			}

			bool asKeyFrame = tracks->asKeyFrame;

			// Push in the measurements
			vio.addKeypoints(okvis::Time::fromMicros(tracks->timestamp), 0,
				dv::data::fromTimedKeyPoints(tracks->keypoints), {}, cv::Mat(), &asKeyFrame);

			// Lastly - visualization, retrieve images, draw tracks and so on
			frameTracks.visualize(frame->image).copyTo(framePreview);
			auto intermediateFrames = frameTracker->getAccumulatedFrames();
			if (!intermediateFrames.empty()) {
				cv::cvtColor(intermediateFrames.back().pyramid.front(), eventPreview, cv::COLOR_GRAY2BGR);
				const auto eventFeatures = frameTracker->getEventTrackPoints();
				for (const auto &point : eventFeatures.back()) {
					cv::drawMarker(eventPreview, point, dvv::colors::magenta, cv::MARKER_SQUARE, 3);
				}
			}
			cv::imshow("Preview", preview);
			int keypress = cv::waitKey(1);

			cv::imshow("Trajectory", visualizer.generateFrame().image);
			cv::waitKey(keypress == 'p' ? 0 : 20);
		}
	}

	return EXIT_SUCCESS;
}

CameraPtr fromIntrinsicVector(const Eigen::VectorXd &intrinsics, const cv::Size &resolution) {
	std::vector<float> distortion;

	for (Eigen::Index i = 4; i < intrinsics.size(); i++) {
		distortion.push_back(static_cast<float>(intrinsics(i)));
	}

	return std::make_shared<dv::camera::CameraGeometry>(distortion, intrinsics(0), intrinsics(1), intrinsics(2),
		intrinsics(3), resolution, dv::camera::DistortionModel::RadTan);
}
