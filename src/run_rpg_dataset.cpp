#include <dv-processing/data/utilities.hpp>
#include <dv-processing/features/event_combined_lk_tracker.hpp>
#include <dv-processing/features/feature_tracks.hpp>
#include <dv-processing/features/image_feature_lk_tracker.hpp>
#include <dv-processing/kinematics/motion_compensator.hpp>
#include <dv-processing/visualization/pose_visualizer.hpp>

#include "dataset/EventDatasetReader.hpp"

#include <CLI/CLI.hpp>
#include <boost/lockfree/spsc_queue.hpp>

#include <cstdlib>
#include <filesystem>
#include <okvis/ThreadedKFVio.hpp>
#include <okvis/VioParametersReader.hpp>

// Namespace aliases
namespace dvf = dv::features;
namespace dvk = dv::kinematics;
namespace dvv = dv::visualization;
namespace fs  = std::filesystem;

// Type aliases
using DepthQueue     = boost::lockfree::spsc_queue<std::pair<int64_t, float>>;
using PoseQueue      = boost::lockfree::spsc_queue<dvk::Transformationf>;
using TrackingResult = dvf::ImageFeatureLKTracker::Result::ConstPtr;
using CameraPtr      = dv::camera::CameraGeometry::SharedPtr;

// Create okvis time from microsecond timestamp
okvis::Time fromMicros(int64_t timestamp);

// Create dv camera geometry class from intrinsics vector
CameraPtr fromIntrinsicVector(const Eigen::VectorXd &intrinsics, const cv::Size &resolution);

int main(int ac, char **av) {
	std::string datasetPath;
	std::string configPath;

	// Configuration parameters
	CLI::App app{"OKVIS Visual-inertial odometry on DAVIS data"};

	app.add_option("-d,--dataset", datasetPath, "Path a RPG DAVIS dataset.");
	app.add_option("-c,--config", configPath,
		"Path to yaml config file containing camera calibration and "
		"OKVIS algorithm parameters.");
	try {
		app.parse(ac, av);
	}
	catch (const CLI::ParseError &e) {
		return app.exit(e);
	}

	// Validate the arguments
	if (datasetPath.empty()) {
		std::cerr << "Dataset path was not set, please make sure dataset path is "
					 "passed into the arguments and is valid."
				  << std::endl;
		return EXIT_FAILURE;
	}

	if (!fs::exists(datasetPath)) {
		std::cerr << "The provided dataset path [" << datasetPath
				  << "] does not exists in the file system, please check the path." << std::endl;
		return EXIT_FAILURE;
	}

	if (configPath.empty()) {
		std::cerr << "Configuration file path was not set, please make sure the "
					 "configuration file path is "
					 "passed into the arguments and is valid."
				  << std::endl;
		return EXIT_FAILURE;
	}

	if (!fs::exists(configPath) || fs::path(configPath).extension() != ".yaml") {
		std::cerr << "The provided configuration file path [" << configPath
				  << "] does not exists in the file system or is not an '.yaml' "
					 "file, please check the path."
				  << std::endl;
		return EXIT_FAILURE;
	}

	// Read the configuration parameters from the yaml file
	okvis::VioParametersReader vio_parameters_reader(configPath);
	okvis::VioParameters parameters;
	vio_parameters_reader.getParameters(parameters);

	// Initialize the OKVIS VIO pipeline
	okvis::ThreadedKFVio vio(parameters);
	vio.setBlocking(true);

	// IMU->Camera transformation
	auto T_SC = parameters.nCameraSystem.T_SC(0);

	// Queue up the pose estimates from OKVIS pipeline, we will need them later on
	PoseQueue poseQueue(10000);
	vio.setStateCallback(
		[&T_SC, &poseQueue](const okvis::Time &timestamp, const okvis::kinematics::Transformation &T_WS) {
			dvk::Transformationf transform(
				static_cast<int64_t>(timestamp.toSec() * 1e+6), (T_WS.T() * T_SC->T()).cast<float>());
			poseQueue.push(transform);
		});

	// Depth is estimated by using the median depth of current landmarks
	// Queue them up, they will be used later on
	DepthQueue depthQueue(1000);
	vio.setLandmarksCallback([&depthQueue](const okvis::Time &timestamp, const okvis::MapPointVector &landmarks,
								 const okvis::MapPointVector &marginalizedLandmarks) {
		if (!landmarks.empty()) {
			std::vector<double> depths;
			depths.reserve(landmarks.size());
			for (const auto &landmark : landmarks) {
				depths.emplace_back(landmark.distance);
			}
			std::sort(depths.begin(), depths.end());
			double medianDepth = depths[depths.size() / 2];
			auto time_us       = static_cast<int64_t>(timestamp.toSec() * 1e+6);
			depthQueue.push(std::make_pair(time_us, static_cast<float>(medianDepth)));
		}
	});

	// RPG format dataset reader
	test::EventDatasetReader reader(datasetPath);

	// Parse the camera geometry
	const auto &cameraGeometry = parameters.nCameraSystem.cameraGeometry(0);

	// Sensor resolution
	cv::Size sensorResolution(
		static_cast<int>(cameraGeometry->imageWidth()), static_cast<int>(cameraGeometry->imageHeight()));

	// Retrieve the camera calibration
	Eigen::VectorXd intrinsics;
	cameraGeometry->getIntrinsics(intrinsics);

	// Custom function to build dv::processing camera geometry class
	auto camera = fromIntrinsicVector(intrinsics, sensorResolution);

	// Build the trackers
	auto frameTracker = dvf::EventCombinedLKTracker<dvk::MotionCompensator<>>::MotionAwareTracker(camera);
	// This can be increased
	frameTracker->setMaxTracks(100);

	// Track history and visualization windows
	dvf::FeatureTracks frameTracks;
	dvf::FeatureTracks eventTracks;
	cv::namedWindow("Preview", cv::WINDOW_NORMAL);
	cv::namedWindow("Trajectory", cv::WINDOW_AUTOSIZE);

	// Preview image, horizontal concat for image and event motion-comensated
	// frames
	cv::Mat preview(sensorResolution.height, sensorResolution.width * 2, CV_8UC3);
	cv::Mat framePreview = preview(cv::Rect(0, 0, sensorResolution.width, sensorResolution.height));
	cv::Mat eventPreview
		= preview(cv::Rect(sensorResolution.width, 0, sensorResolution.width, sensorResolution.height));

	// Amount of time in microseconds, the imu will be 5ms in front of the frame
	int64_t imuOverheadDuration = 5000;

	// Camera pose estimation visualization
	dvv::PoseVisualizer visualizer(1000000);
	visualizer.setViewMode(dvv::PoseVisualizer::Mode::VIEW_ZX);
	visualizer.setGridPlane(dvv::PoseVisualizer::GridPlane::PLANE_ZX);

	// Sequential read of images
	while (auto image = reader.readNextImage()) {
		// Push IMU data with an overhead
		auto imuData = reader.readNextImuUntil(image->first + imuOverheadDuration);
		for (const auto &imu : imuData) {
			vio.addImuMeasurement(fromMicros(imu.timestamp),
				Eigen::Vector3d(static_cast<const double>(imu.accelerometerX),
					static_cast<const double>(imu.accelerometerY), static_cast<const double>(imu.accelerometerZ)),
				Eigen::Vector3d(static_cast<const double>(imu.gyroscopeX), static_cast<const double>(imu.gyroscopeY),
					static_cast<const double>(imu.gyroscopeZ)));
		}

		// Retrieve asynchronously computed poses and camera depths
		// These are required for motion compensation
		poseQueue.consume_all([&frameTracker, &visualizer](const dvk::Transformationf &T_WC) {
			frameTracker->accept(T_WC);
			visualizer.accept(T_WC);
		});
		depthQueue.consume_all([&frameTracker](const std::pair<int64_t, float> &depth) {
			dv::measurements::Depth depthMeasurement(depth.first, depth.second);
			frameTracker->accept(depthMeasurement);
		});

		// Track on an image and estimate pose
		frameTracker->accept(dv::Frame(image->first, image->second));
		// Read events until the image timestamp, events are used for intermediate
		// image generation using motion compensation, it uses these
		// frame to predict locations of the tracks on image frame
		frameTracker->accept(reader.readEventsUntil(image->first));
		if (auto tracks = frameTracker->runTracking(); tracks != nullptr) {
			frameTracks.accept(tracks);

			bool asKeyFrame = tracks->asKeyFrame;

			// Push in the measurements
			vio.addKeypoints(fromMicros(tracks->timestamp), 0, dv::data::fromTimedKeyPoints(tracks->keypoints), {},
				cv::Mat(), &asKeyFrame);

			// Lastly - visualization, retrieve images, draw tracks and so on
			frameTracks.visualize(image->second).copyTo(framePreview);
			auto intermediateFrames = frameTracker->getAccumulatedFrames();
			if (!intermediateFrames.empty()) {
				cv::cvtColor(intermediateFrames.back().pyramid.front(), eventPreview, cv::COLOR_GRAY2BGR);
				const auto eventFeatures = frameTracker->getEventTrackPoints();
				for (const auto &point : eventFeatures.back()) {
					cv::drawMarker(eventPreview, point, dvv::colors::magenta, cv::MARKER_SQUARE, 3);
				}
			}
			cv::imshow("Preview", preview);
			cv::waitKey(1);

			cv::imshow("Trajectory", visualizer.generateFrame().image);
			cv::waitKey(1);
		}
	}

	return EXIT_SUCCESS;
}

okvis::Time fromMicros(int64_t timestamp) {
	return okvis::Time(static_cast<double>(timestamp) * 1e-6);
}

CameraPtr fromIntrinsicVector(const Eigen::VectorXd &intrinsics, const cv::Size &resolution) {
	std::vector<float> distortion;

	for (Eigen::Index i = 4; i < intrinsics.size(); i++) {
		distortion.push_back(static_cast<float>(intrinsics(i)));
	}

	return std::make_shared<dv::camera::CameraGeometry>(distortion, intrinsics(0), intrinsics(1), intrinsics(2),
		intrinsics(3), resolution, dv::camera::DistortionModel::RadTan);
}
